# training <- read.csv("E:/banjir/train.csv")
# testing <- read.csv("E:/banjir/test.csv")
args <- commandArgs(TRUE)
train_file<-args[1]
test_file<-args[2]
training <- read.csv(train_file)
testing <- read.csv(test_file)
  X1Data <- training$tuntungan
  X2Data <- training$sampali
  X3Data <- training$knamu
  X4Data <- training$debit
  YData <- training$banjir


  X1minData <- min(X1Data)
  X1maxData <- max(X1Data) 
  X1normalisasiData <- (X1Data - X1minData)/(X1maxData - X1minData)

  X2minData <- min(X2Data)
  X2maxData <- max(X2Data) 
  X2normalisasiData <- (X2Data - X2minData)/(X2maxData - X2minData)

  X3minData <- min(X3Data)
  X3maxData <- max(X3Data) 
  X3normalisasiData <- (X3Data - X3minData)/(X3maxData - X3minData)

  X4minData <- min(X4Data)

  X4maxData <- max(X4Data) 
  X4normalisasiData <- (X4Data - X4minData)/(X4maxData - X4minData)

 
xData <- cbind(X1normalisasiData,X2normalisasiData,X3normalisasiData,X4normalisasiData)
  sum.Data <- data.frame(xData,YData)


library(e1071)
modelSVR <- svm(x=as.matrix(xData),y=sum.Data$YData,
			kernel="radial")


  X1test <- testing$tuntungan
  X2test <- testing$sampali
  X3test <- testing$knamu
  X4test <- testing$debit
#  Ytest <- testing$banjir

 
X1normalisasitest <- (X1test - X1minData)/(X1maxData - X1minData)
X2normalisasitest <- (X2test - X2minData)/(X2maxData - X2minData)
X3normalisasitest <- (X3test - X3minData)/(X3maxData - X3minData)
X4normalisasitest <- (X4test - X4minData)/(X4maxData - X4minData)


xTest<- cbind(X1normalisasitest,X2normalisasitest,X3normalisasitest,X4normalisasitest)
  sum.DataTest <- data.frame(xTest)

resultSVR <- predict(modelSVR,xTest)

pathToFileResultCSV <- "banjir/resultSVR.csv"
write.table(resultSVR, file=pathToFileResultCSV,sep = "\t",row.names = F, col.names = F)

pathToFileResultTXT <- "banjir/resultSVR.txt"
write.table(resultSVR, file=pathToFileResultTXT,sep = "\t",row.names = F, col.names = F)




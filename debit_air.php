
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    </head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <center><h1>Data Results</h1></center>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
        <img src="debit_air/Rplot.png"/><br>
        </div>
        <div class="col-md-3">
        <?php
                $myfile_new = fopen("debit_air/result.txt", "r") or die("Unable to open file!");
                $myfile_ytest = fopen("debit_air/yTest.txt", "r") or die("Unable to open file!");
                $error_result = array();
                $denorm_result = array();
                while(!feof($myfile_new)) {
                    $text = fgets($myfile_new);
                    $text_y = fgets($myfile_ytest);
                    $nilai = floatval($text);
                    $nilai_y = floatval($text_y);
                    
                    if($nilai<0){
                        $value = 0;
                    }else{
                        $value = $nilai;
                    }
                    if($text!=""){
                        $error = $nilai_y - $value;
                        // echo($value." & ".$error." & ".$error*$error."<br>");
                        echo($value."<br>");
                        array_push($error_result, $error*$error);
                        array_push($denorm_result, $value."\n");
                    }
                }
                fclose($myfile_new);
                fclose($myfile_ytest);

                $file = file("debit_air/result.txt");
                $number = count($file);
                $denorm_data = "";
                $sum_error_2 = 0;
                for ($i = 0; $i <$number; $i++) {
                    $denorm_data = $denorm_data . $denorm_result[$i];
                    $sum_error_2 = $sum_error_2 + $error_result[$i];
                 }
                 $result = $sum_error_2/($number);
                 $rmse = sqrt($result);
                //  echo($rmse);
                 $denorm_file = fopen("debit_air/denorm.csv", "w") or die("Unable to open file!");
                 fwrite($denorm_file, $denorm_data);
                 $rmse_file = fopen("debit_air/RMSE.txt", "w") or die("Unable to open file!");
                 fwrite($rmse_file, $rmse);
                 fclose($denorm_file);
                 fclose($rmse_file);
            ?>
    </div>
        <div class="col-md-3">
        <?php    
    echo ("RMSE     : "),file_get_contents("debit_air/RMSE.txt");
    ?>
        </div>
        </div>
    </div>
</div>
</body>
</html>
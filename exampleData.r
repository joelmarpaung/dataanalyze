args <- commandArgs(TRUE)
train_file<-args[1]
test_file<-args[2]
training <- read.csv(train_file)
testing <- read.csv(test_file)
  X0Data <- training$Tanggal
  X1Data <- training$suhu.minimum
  X2Data <- training$suhu.maksimum
  X3Data <- training$suhu.rata.rata
  X4Data <- training$kelembaban.rata.rata
  X5Data <- training$lama.penyinaran
  X6Data <- training$kecepatan.angin.rata.rata
  YData <- training$curah.hujan


  X1minData <- min(X1Data)
  X1maxData <- max(X1Data) 
  X1normalisasiData <- (X1Data - X1minData)/(X1maxData - X1minData)

  X2minData <- min(X2Data)
  X2maxData <- max(X2Data) 
  X2normalisasiData <- (X2Data - X2minData)/(X2maxData - X2minData)

  X3minData <- min(X3Data)
  X3maxData <- max(X3Data) 
  X3normalisasiData <- (X3Data - X3minData)/(X3maxData - X3minData)

  X4minData <- min(X4Data)

  X4maxData <- max(X4Data) 
  X4normalisasiData <- (X4Data - X4minData)/(X4maxData - X4minData)

  X5minData <- min(X5Data)
  X5maxData <- max(X5Data) 
  X5normalisasiData <- (X5Data - X5minData)/(X5maxData - X5minData)


  X6minData <- min(X6Data)
  X6maxData <- max(X6Data) 
  X6normalisasiData <- (X6Data - X6minData)/(X6maxData - X6minData)

  YmintargetData <- min(YData)
  YmaxtargetData <- max(YData)
  YnormalisasiData <- (YData - YmintargetData)/(YmaxtargetData - YmintargetData)
 
xData <- cbind(X1normalisasiData,X2normalisasiData,X3normalisasiData,X4normalisasiData,X5normalisasiData,X6normalisasiData)
  sum.Data <- data.frame(xData,YnormalisasiData)

library(neuralnet)
modelBPNN <- neuralnet(formula = YnormalisasiData ~ X1normalisasiData+X2normalisasiData+X3normalisasiData+X4normalisasiData+X5normalisasiData+X6normalisasiData,
                     data = sum.Data,linear.output = T)

  

  X1test <- testing$suhu.minimum
  X2test <- testing$suhu.maksimum
  X3test <- testing$suhu.rata.rata
  X4test <- testing$kelembaban.rata.rata
  X5test <- testing$lama.penyinaran
  X6test <- testing$kecepatan.angin.rata.rata
  Ytest <- testing$curah.hujan

 
X1normalisasitest <- (X1test - X1minData)/(X1maxData - X1minData)
X2normalisasitest <- (X2test - X2minData)/(X2maxData - X2minData)
X3normalisasitest <- (X3test - X3minData)/(X3maxData - X3minData)
X4normalisasitest <- (X4test - X4minData)/(X4maxData - X4minData)
X5normalisasitest <- (X5test - X5minData)/(X5maxData - X5minData)
X6normalisasitest <- (X6test - X6minData)/(X6maxData - X6minData)
Ynormalisasitest <- (Ytest - YmintargetData)/(YmaxtargetData - YmintargetData)

xTest<- cbind(X1normalisasitest,X2normalisasitest,X3normalisasitest,X4normalisasitest,X5normalisasitest,X6normalisasitest)
  sum.DataTest <- data.frame(xTest,Ynormalisasitest)

resultBPNN <- compute(modelBPNN,xTest)
 c <-resultBPNN$net.result

DenormalisasiBPNN <- (c * (YmaxtargetData - YmintargetData))+ YmintargetData

error<-Ytest-DenormalisasiBPNN
RMSE<-sqrt(sum(error^2)/length(error))

png(file="curah_hujan/Rplot.png", width=600, height=350)
plot(X0Data,YData, main="Grafik Curah hujan", pch = 16, frame = FALSE, xlab="Tanggal", ylab="Curah Hujan")
dev.off()

DenormalisasiBPNN
pathToFileResult <- "curah_hujan/result.txt"
write.table(DenormalisasiBPNN, file=pathToFileResult,sep = "\t",row.names = F)

RMSE
pathToFileRMSE <- "curah_hujan/resultRMSE.txt"
write.table(RMSE, file=pathToFileRMSE,sep = "\t",row.names = F, col.names = F)

# Plot
# pathToFilePlot <- "/home/edwaresultBPNNrd/resultPlot.txt"
# write.table(Plot, file=pathToFilePlot,sep = "\t", row.names = F, col.names = " ")


args <- commandArgs(TRUE)
train_file<-args[1]
test_file<-args[2]
training <- read.csv(train_file)
testing <- read.csv(test_file)
  X0Data <- testing$Tanggal
  X1Data <- training$debit.x
  YData <- training$debit.y


  X1minData <- min(X1Data)
  X1maxData <- max(X1Data) 
  X1normalisasiData <- (X1Data - X1minData)/(X1maxData - X1minData)

  YmintargetData <- min(YData)
  YmaxtargetData <- max(YData)
  YnormalisasiData <- (YData - YmintargetData)/(YmaxtargetData - YmintargetData)
 
xData <- cbind(X1normalisasiData)
  sum.Data <- data.frame(xData,YnormalisasiData)

library(neuralnet)
modelBPNN <- neuralnet(formula = YnormalisasiData ~ X1normalisasiData,
                     data = sum.Data, learningrate=0.5,
                     threshold = 0.01,
                     hidden = 7, err.fct = "sse", act.fct = "logistic",
                     linear.output = T)

  

  X1test <- testing$debit.x
  Ytest <- testing$debit.y

 
X1normalisasitest <- (X1test - X1minData)/(X1maxData - X1minData)
Ynormalisasitest <- (Ytest - YmintargetData)/(YmaxtargetData - YmintargetData)

xTest<- cbind(X1normalisasitest)
  sum.DataTest <- data.frame(xTest,Ynormalisasitest)

resultBPNN <- compute(modelBPNN,xTest)
 c <-resultBPNN$net.result

DenormalisasiBPNN <- (c * (YmaxtargetData - YmintargetData))+ YmintargetData

error<-Ytest-DenormalisasiBPNN
RMSE<-sqrt(sum(error^2)/length(error))

png(file="debit_air/Rplot.png", width=600, height=350)
plot(X0Data,Ytest, main="Grafik Curah hujan", pch = 16, frame = FALSE, xlab="Tanggal", ylab="Curah Hujan")
dev.off()

DenormalisasiBPNN
pathToFileResult <- "debit_air/result.txt"
write.table(DenormalisasiBPNN, file=pathToFileResult,sep = "\t",row.names = F, , col.names = F)

RMSE
pathToFileRMSE <- "debit_air/resultRMSE.txt"
write.table(RMSE, file=pathToFileRMSE,sep = "\t",row.names = F, col.names = F)

Ytest
pathToFileYTest <- "debit_air/yTest.txt"
write.table(Ytest, file=pathToFileYTest,sep = "\t",row.names = F, col.names = F)
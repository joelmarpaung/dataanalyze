<?php
include_once('functions.php'); 
if ( isset($_POST["submit"]) ) {
    $training = "train.csv";
    $testing = "test.csv";
    $banjir = "banjir.csv";
    move_uploaded_file($_FILES["training"]["tmp_name"], "banjir/upload/" . $training);
    move_uploaded_file($_FILES["testing"]["tmp_name"], "banjir/upload/" . $testing);
    move_uploaded_file($_FILES["banjir"]["tmp_name"], "banjir/" . $banjir);
    $train_path = realpath("banjir/upload/train.csv");
    $test_path = realpath("banjir/upload/test.csv");
    exec("Rscript svm.r $train_path $test_path");
    redirect('banjir.php');
}
?>
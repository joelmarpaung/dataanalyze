<?php
include_once('functions.php'); 
if ( isset($_POST["submit"]) ) {

if ( isset($_FILES["file"])) {
     if ($_FILES["file"]["error"] > 0) {
         echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
     }
     else {
         $train = $_POST["train"];
         $amount = $_POST["amount"];
         $test = 100 - $train;
         $standard_name = date("Y-m-d")."_".date("h-i-s");
         mkdir ("upload/".$standard_name, 0755);
         $data_name = $standard_name."_".$amount."-".$train."-".$test.".csv";
         $split_name = $standard_name."_split_".$amount.".csv";
         $train_name = $standard_name."_train_".$train.".csv";
         $test_name = $standard_name."_test_".$test.".csv";
         move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $standard_name."/". $data_name);

         $data_file = file("upload/" . $standard_name."/". $data_name);
         $myfile = fopen("upload/" . $standard_name."/". $data_name, "r") or die("Unable to open file!");
         $data_x_bef_date = array();
         $data_x_date = array();
         $data_x_aft_date = array();
         $data_y = array();
         while(! feof($myfile)){
            $ar=fgetcsv($myfile);
            $x_bef_date = $ar[0].",".$ar[1].",".$ar[2];
            array_push($data_x_bef_date, $x_bef_date);
            $x_date = $ar[3];
            array_push($data_x_date, $x_date);
            $x_aft_date = $ar[4].",".$ar[5].",".$ar[6].",".$ar[7].",".$ar[8].",".$ar[9];
            array_push($data_x_aft_date, $x_aft_date);
            $y = $ar[10];
            array_push($data_y, $y);
        }
        $number = count($data_file)-1;
        $num_amount = $number - $amount;
        $data_split = $data_x_bef_date[0].",".$data_x_date[0].",".$data_x_aft_date[0].",".$data_y[0]."\n";
        for ($i = 1; $i <= $num_amount; $i++) {
            $data_split = $data_split . $data_x_bef_date[$i].",".$data_x_date[$i+$amount].",".$data_x_aft_date[$i].",".$data_y[$i+$amount]."\n";
        }
        $split_file = fopen("upload/" . $standard_name."/". $split_name, "w") or die("Unable to open file!");
        fwrite($split_file, $data_split);
        fclose($split_file);
        $data_file = file("upload/" . $standard_name."/". $split_name);
        $number = count($data_file)-1;
         $train = $train/100;
         $train_sum = round($number * $train);
         $test_sum = $number - $train_sum;
         $train_data = $data_file[0];
         $test_data = $data_file[0];
         for ($i = 1; $i <= $train_sum; $i++) {
            $train_data = $train_data . $data_file[$i];
         }
         for ($i = $train_sum+1; $i <= $number; $i++) {
            $test_data = $test_data . $data_file[$i];
         }
         $train_file = fopen("upload/" . $standard_name."/". $train_name, "w") or die("Unable to open file!");
         $test_file = fopen("upload/" . $standard_name."/". $test_name, "w") or die("Unable to open file!");
         fwrite($train_file, $train_data);
         fwrite($test_file, $test_data);
         
         fclose($test_file);
         fclose($train_file);
         $train_path= realpath("upload/" . $standard_name."/". $train_name);
         $test_path= realpath("upload/" . $standard_name."/". $test_name);
         exec("Rscript newR.r $train_path $test_path");
     }
  } else {
          echo "No file selected <br />";
  }
  redirect('curah_hujan.php');
}
?>
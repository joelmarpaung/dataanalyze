<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    </head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <center><h1>Data Results</h1></center>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
        <div class="row">
        <center><h3>Prediction Results</h3></center>
        </div>
        <div class="row">
        <div class="col-md-5">
        <?php
                $myfile_new = fopen("banjir/resultSVR.txt", "r") or die("Unable to open file!");
                $predict = array();
                $yes = 0;
                $no = 0;
                while(!feof($myfile_new)) {
                    $text = fgets($myfile_new);
                    $text = trim($text);
                    $text = trim($text,'"');
                    echo($text);
                    if($text == "No"){
                        $no++;
                    }else if($text == "Yes"){
                        $yes++;
                    }
                    array_push($predict, $text);
                    echo("<br>");
                }
                fclose($myfile_new);
            ?>
            </div>
            <div class="col-md-7">
            <?php
                echo("Jumlah Yes : ");
                echo($yes);
                echo("<br>");
                echo("Jumlah No : ");
                echo($no);
            ?>
        </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
        <center><h3>Real Results</h3></center>
            </div>
        <div class="row">
            <div class="col-md-5">
        <?php
                $myfile = fopen("banjir/banjir.csv", "r") or die("Unable to open file!");
                $real = array();
                $yes = 0;
                $no = 0;
                while(!feof($myfile)) {
                    $text = fgets($myfile);
                    $text = trim($text);
                    $text = trim($text,'"');
                    echo($text);
                    if($text == "No"){
                        $no++;
                    }else if($text == "Yes"){
                        $yes++;
                    }
                    echo("<br>");
                    array_push($real, $text);
                }
                fclose($myfile);
            ?>
            </div>
            <div class="col-md-7">
            <?php
                echo("Jumlah Yes : ");
                echo($yes);
                echo("<br>");
                echo("Jumlah No : ");
                echo($no);
            ?>
        </div>
            </div>
        </div>
        <div class="col-md-2">
        <center><h3>Pembahasan</h3></center>
        <?php 
            $benar = 0;
            $salah = 0;
            $arrlength = count($real)-1;
            for($x = 0; $x < $arrlength; $x++) {
                if($predict[$x]==$real[$x]){
                    $benar++;
                }else{
                    $salah++;
                }
            }
        ?>
        <label>Jumlah Benar : <?php echo($benar); ?> </label>
        <label>Jumlah Salah : <?php echo($salah); ?></label>
        <label>Total Data : <?php echo($arrlength); ?></label>
        <label>Akurasi : <?php echo(number_format((($benar/$arrlength)*100),3)); ?>%</label>
        </div>
        </div>
    </div>
    
</div>
</body>
</html>